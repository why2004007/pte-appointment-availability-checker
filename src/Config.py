# -*- coding: utf-8 -*-
# Created by Daniel Liu on 2017/7/25

import configparser
import os


class Config():
    def __init__(self, configFileName, log):
        self.log = log
        self.parser = configparser.ConfigParser(allow_no_value=True)
        # Set options to be case sensitive
        self.parser.optionxform = str
        # Create default config file
        if not os.path.exists(configFileName):
            self.log.error("Config file does not exist, create default one.")
            self.parser.add_section("General")
            self.parser.set("General", "PTE Username", "")
            self.parser.set("General", "PTE Password", "")
            self.parser.set("General", "DEBUG", "False")
            self.parser.set("General",
                            "; Once this option is enable, the software will only report current month's events")
            self.parser.set("General", "Only Report Current Month", "True")
            self.parser.set("General",
                            "; Once this option is enable, it will overwrite previous settings, it will only "
                            "report specific month's events as defined.")
            self.parser.set("General", "Only Report Specific Month", "False")
            self.parser.set("General",
                            "; This option will not be effect unless Only Report Specific Month is set to True")
            self.parser.set("General", "Specific Month", "1")
            self.parser.set("General", "; PTE test takers can not book exam within 24h.")
            self.parser.set("General", "Report Appointment within 24h", "False")
            self.parser.set("General", "; The recommended intervals is 300s, this is enough time for the browser to "
                                       "retrieve and run scripts, lower than this value will cause unexpected errors.")
            self.parser.set("General", "Scan Intervals(s)", "300")
            self.parser.set("General", "Log Level", "INFO")
            self.parser.add_section("Mail Settings")
            self.parser.set("Mail Settings",
                            "; The following part will define which account will be used to send notifications.")
            self.parser.set("Mail Settings", "; Put email address here. Currently only support Gmail.")
            self.parser.set("Mail Settings", "Username", "")
            self.parser.set("Mail Settings", "Password", "")
            self.parser.set("Mail Settings", "NickName", "")
            self.parser.set("Mail Settings", "Subject", "")
            self.parser.set("Mail Settings", "; The running stats will be sent to the following email address.")
            self.parser.set("Mail Settings",
                            "; If DEBUG is enabled, only the administrator will receive notifications.")
            self.parser.set("Mail Settings", "Administrator's Email", "")
            self.parser.set("Mail Settings", "Administrator Accept Notifications", "True")
            self.parser.set("Mail Settings", "Receiver's Email", "")
            with open(configFileName, "w+") as fp:
                self.parser.write(fp)
            self.log.error("Please fill up info in the config file.")
            os._exit(0)
        # Load Config
        self.parser.read(configFileName)

        # Setup Logger with Proper Level
        logLevelStr = self.__get("General", "Log Level")
        if self.isDebug():
            logLevelStr = "DEBUG"
        self.log.setLogLevel(self.log.getLogLevelFromString(logLevelStr))
        self.log.debug("Log level has been set to {0}".format(logLevelStr))

        self.log.debug("Config file loaded.")

    def checkConfigIntegrity(self):
        try:
            self.parser.get("General", "PTE Username")
            self.parser.get("General", "PTE Password")
            self.parser.get("General", "DEBUG")
            self.parser.get("General", "Only Report Current Month")
            self.parser.get("General", "Only Report Specific Month")
            self.parser.get("General", "Specific Month")
            self.parser.get("General", "Scan Intervals(s)")
            self.parser.get("Mail Settings", "Username")
            self.parser.get("Mail Settings", "Password")
            self.parser.get("Mail Settings", "NickName")
            self.parser.get("Mail Settings", "Subject")
            self.parser.get("Mail Settings", "Administrator's Email")
            self.parser.get("Mail Settings", "Administrator Accept Notifications")
            self.parser.get("Mail Settings", "Receiver's Email")
        except:
            self.log.error("Config file is corrupted. Please check it.")
            os._exit(1)

    def __getBoolean(self, section, option):
        try:
            return self.parser.getboolean(section, option)
        except:
            self.log.error("{0} in section {1} is not properly set. Only \"True\" and \"False\" are accepted.")
            os._exit(1)

    def __get(self, section, option):
        try:
            return self.parser.get(section, option)
        except:
            self.log.error("{0} in section {1} is not properly set. Only string is accepted.")
            os._exit(1)

    def __getInt(self, section, option):
        try:
            return self.parser.getint(section, option)
        except:
            self.log.error("{0} in section {1} is not properly set. Only integers are accepted.")

    def getPTEAuth(self):
        username = self.__get("General", "PTE Username")
        password = self.__get("General", "PTE Password")
        return username, password

    def isDebug(self):
        return self.__getBoolean("General", "DEBUG")

    def isReportWithin24HoursAppointments(self):
        return self.__getBoolean("General", "Report Appointment within 24h")

    def isOnlyReportCurrentMonth(self):
        return self.__getBoolean("General", "Only Report Current Month")

    def isOnlyReportSpecificMonth(self):
        return self.__getBoolean("General", "Only Report Specific Month")

    def getSpecificMonth(self):
        return self.__getInt("General", "Specific Month")

    def getScanIntervals(self):
        return self.__getInt("General", "Scan Intervals(s)")

    def getMailAuth(self):
        username = self.__get("Mail Settings", "Username")
        password = self.__get("Mail Settings", "Password")
        return username, password

    def getSenderNickname(self):
        return self.__get("Mail Settings", "NickName")

    def getMailSubject(self):
        return self.__get("Mail Settings", "Subject")

    def getAdminMail(self):
        return self.__get("Mail Settings", "Administrator's Email")

    def getReceivers(self):
        receiversStr = self.__get("Mail Settings", "Receiver's Email")
        return receiversStr.split(",")

    def isAdminReceiveNotification(self):
        return self.__getBoolean("Mail Settings", "Administrator Accept Notifications")
