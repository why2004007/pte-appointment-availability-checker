# -*- coding: utf-8 -*-
# Created by Daniel Liu on 2017/7/25
import datetime
import time
import traceback

from Browser import Browser
from Config import Config
from MailAgent import MailAgent
from lib.Log import Log

reportedDates = {'RMIT': [], 'PEARSON': [], 'CLIFTON': []}


def compareDate(reported, result):
    return reported["year"] == result["year"] and \
           reported["month"] == result["month"] and \
           reported["day"] == result["day"]


def processRetrievedData(data, config, log):
    message = "Available Dates: \n"
    needReport = False
    for testCenter in ["RMIT", "PEARSON", "CLIFTON"]:
        # First remove disappeared dates
        for reportedDate in reportedDates[testCenter]:
            needRemove = True
            for result in data[testCenter]:
                if compareDate(reportedDate, result):
                    needRemove = False
            if needRemove:
                reportedDates[testCenter].remove(reportedDate)
        # Then determine whether there are dates that needs to be reported
        tomorrow = datetime.date.today() + datetime.timedelta(1)
        for result in data[testCenter]:
            if config.isOnlyReportSpecificMonth() and result['month'] != config.getSpecificMonth():
                log.info('Not Desired Date: {0}: {1}-{2}-{3}'.format(
                    testCenter, result["year"], result["month"], result["day"]))
                continue
            if config.isOnlyReportCurrentMonth() and result["month"] != tomorrow.month:
                log.info('Not Desired Date: {0}: {1}-{2}-{3}'.format(
                    testCenter, result["year"], result["month"], result["day"]))
                continue
            if (not config.isReportWithin24HoursAppointments()) and result['day'] == tomorrow.day + 1:
                log.info('Within 24 Hours: {0}: {1}-{2}-{3}'.format(
                    testCenter, result["year"], result["month"], result["day"]))
                continue
            message += "{0}: {1}-{2}-{3} \n".format(testCenter, result["year"], result["month"], result["day"])
            if len(reportedDates[testCenter]) == 0:
                reportedDates[testCenter].append(result)
                needReport = True
            for reportedDate in reportedDates[testCenter]:
                if compareDate(reportedDate, result):
                    continue
                else:
                    reportedDates[testCenter].append(result)
                    needReport = True
    return message, needReport

def main():
    logger = Log("PTE-ACC", "PTEChecker.log", "Logs")
    config = Config("PTEChecker.conf", logger)
    while True:
        try:
            mailAgent = MailAgent(config, logger)
            browser = Browser(config, logger)
            data = browser.startRetrieveData()
            message, needReport = processRetrievedData(data, config, logger)
            if needReport:
                mailAgent.sendEmail(message)
                logger.debug('Message has been sent.')
            if not needReport:
                logger.info('No desired dates.')
            logger.info('Routine check finished.')
            time.sleep(config.getScanIntervals())

        except Exception as e:
            logger.error('Error occurred: {0}'.format(e))
            logger.error(traceback.format_exc())
            continue


if __name__ == '__main__':
    main()
