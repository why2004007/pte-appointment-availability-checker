# -*- coding: utf-8 -*-
# Created by Daniel Liu on 2017/7/25
import os
import platform

from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions


class Browser():
    def __init__(self, config, log):
        self.driver = None
        self.log = log
        self.config = config
        self.TIMEOUT = 60

    def init(self):
        chromeOptions = ChromeOptions()
        if platform.system() == "Windows":
            chromeOptions.binary_location = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
        elif platform.system() == "Linux":
            chromeOptions.binary_location = "/usr/bin/google-chrome-unstable"
            chromeOptions.add_argument("headless")
            chromeOptions.add_argument("disable-gpu")
            chromeOptions.add_argument("--disable-gpu")
        elif platform.system() == "Darwin":
            pass
        else:
            self.log.error("Unsupported OS, consider change to Windows or Linux.")
            os._exit(1)
        if platform.system() == "Windows":
            self.driver = Chrome(chrome_options=chromeOptions,
                                 executable_path=os.path.join("..\\Dependency", "chromedriver.exe"))
        else:
            self.driver = Chrome(chrome_options=chromeOptions)
        self.driver.set_script_timeout(self.TIMEOUT)
        self.driver.set_page_load_timeout(self.TIMEOUT)
        self.driver.start_client()
        self.driver.get("http://pearsonvue.com/pte/activity/")

    def login(self):
        username, password = self.config.getPTEAuth()
        self.driver.execute_script(
            '(function(){'
            '   document.getElementById("inputUserName").value = "%s"; '
            '   document.getElementById("inputPassword").value = "%s"; '
            '   document.getElementById("submitButton").click(); '
            '})()' % (username, password))
        self.log.debug('Login finished.')

    def gotoScheduleExamPage(self):
        self.driver.execute_script(
            '(function(){'
            '   document.getElementById("j_id163").click()'
            '})()')
        self.log.debug('Go to schedule exam page finished.')

    def gotoFillUpInformationPage(self):
        self.driver.execute_script(
            '(function(){'
            '   document.getElementById("nextButton").click()'
            '})()')
        self.log.debug('Go to fill up information page finished.')

    def fillUpForm(self):
        self.driver.execute_script(
            '(function(){'
            '   document.getElementById("component1_SELECT_ONE_RADIOBUTTON_3422:0").checked = true;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_2945").selectedIndex = 17;'
            '   document.getElementById("component1_SELECT_ONE_CHECKBOX_2970").checked = true;'
            '   document.getElementById("component1_SELECT_ONE_CHECKBOX_3395").checked = true;'
            '   document.getElementById("component1_SELECT_ONE_CHECKBOX_3016").checked = true;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_3858").selectedIndex = 2;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_4560").selectedIndex = 2;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_3860").selectedIndex = 5;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_3862").selectedIndex = 1;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_4567").selectedIndex = 9;'
            '   document.getElementById("component1_SELECT_ONE_LISTBOX_5155").selectedIndex = 1;'
            '   document.getElementById("nextButton").click();'
            '})()')
        self.log.debug('Form has been filled up.')

    def selectTestCentre(self):
        self.driver.execute_script(
            '(function(){'
            '   a = document.getElementsByName("selectedTestCenters");'
            '   a.forEach(function(item, index){if (item.value == "75570" || item.value == "74481" || item.value == "67712") item.checked = true;});'
            '   document.getElementById("continueTop").click();'
            '})()')
        self.log.debug('Select test centre finished.')

    def getAvailableDates(self):
        self.log.debug('Start retrieve available dates.')
        result = self.driver.execute_async_script(
            '''
            var finished = arguments[0];
            var message = {
                "RMIT": [],
                "PEARSON": [],
                "CLIFTON": []
            };

            function getForEachAbleArray(item) {
                return Array.prototype.slice.call(item, 0);
            };

            function isDialogsDisappeared(callback) {
                divs = getForEachAbleArray(document.getElementsByTagName("div"));
                isShowing = false;
                divs.forEach(function(item, index) {
                    if (item.getAttribute("role") == "dialog") {
                        if (item.style.display != "none") {
                            isShowing = true;
                        }
                    }
                });
                if (!isShowing)
                    callback();
                else
                  setTimeout(function(){isDialogsDisappeared(callback)}, 500);
            };

            function getAvailableDates(position) {
                tds = getForEachAbleArray(document.getElementsByTagName("td"));
                tds.forEach(function(item, index) {
                    if ("data-handler" in item.attributes) {
                        message[position].push({
                            "year": parseInt(item.getAttribute("data-year")),
                            "month": parseInt(item.getAttribute("data-month")) + 1,
                            "day": parseInt(item.innerText.trim())
                        });
                    }
                });
            };

            function getCurrentSelectedSchool() {
                testCenterLists = getForEachAbleArray(document.getElementsByName("calendarForm:selectedTestCenterId"));
                position = "";
                testCenterLists.forEach(function(item, index) {
                    if (item.checked) {
                        testCenterId = item.getAttribute("value");
                        if (testCenterId == "75570")
                            position = "RMIT";
                        if (testCenterId == "67712")
                            position = "PEARSON";
                        if (testCenterId == "74481")
                            position = "CLIFTON";
                    }
                });
                return position;
            };

            function getFirstAvailableDates() {
                getAvailableDates(getCurrentSelectedSchool());
                document.getElementById("calendarForm:selectedTestCenterId:1").click();
            }

            function getSecondAvailableDates() {
                getAvailableDates(getCurrentSelectedSchool());
                document.getElementById("calendarForm:selectedTestCenterId:2").click();
            }

            function getThirdAvailableDates() {
                getAvailableDates(getCurrentSelectedSchool());
                finished(message);
            }
            isDialogsDisappeared(getFirstAvailableDates);
            isDialogsDisappeared(getSecondAvailableDates);
            isDialogsDisappeared(getThirdAvailableDates);
            ''')
        self.log.verbose(result)
        return result

    def startRetrieveData(self):
        self.init()
        self.login()
        self.gotoScheduleExamPage()
        self.gotoFillUpInformationPage()
        self.fillUpForm()
        self.selectTestCentre()
        data = self.getAvailableDates()
        self.shutDown()
        return data

    def shutDown(self):
        self.driver.stop_client()
        self.driver.close()
        self.driver.quit()
