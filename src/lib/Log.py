# -*- coding: utf-8 -*-
# Created by Daniel Liu on 2017/7/25

import datetime
import inspect
import os


class Log():
    # Log Levels
    ERROR = 3
    INFO = 2
    DEBUG = 1
    VERBOSE = 0

    def __init__(self, programName, logFileName, logFilePath):
        self.programName = programName
        self.logFileName = logFileName
        self.logFilePath = logFilePath
        self.__logLevel = self.INFO

    def setLogLevel(self, newLevel=INFO):
        self.__logLevel = newLevel

    def getLogStringFromLogLevel(self, enum):
        if enum == self.INFO:
            return "INFO"
        if enum == self.ERROR:
            return "ERROR"
        if enum == self.DEBUG:
            return "DEBUG"
        if enum == self.VERBOSE:
            return "VERBOSE"

    def getLogLevelFromString(self, string):
        if string == "INFO":
            return self.INFO
        if string == "ERROR":
            return self.ERROR
        if string == "DEBUG":
            return self.DEBUG
        if string == "VERBOSE":
            return self.VERBOSE

    # The real writing function
    def __log(self, type: object, message: object) -> object:
        if type < self.__logLevel:
            return
        today = datetime.datetime.today()
        if not os.path.exists(self.logFilePath):
            os.mkdir(self.logFilePath)
        logFile = os.path.join(self.logFilePath, self.logFileName)
        with open(logFile, "a+") as f:
            f.write('{0}-{1}-{2} {3}:{4}:{5} [{6}] [{7}] {8}\n'.format(
                today.year, today.month, today.day,
                today.hour, today.minute, today.second,
                inspect.stack()[1][3], self.getLogStringFromLogLevel(type), message))

    # Interface Functions
    def info(self, message):
        self.__log(self.INFO, message)

    def debug(self, message):
        self.__log(self.DEBUG, message)

    def error(self, message):
        self.__log(self.ERROR, message)

    def verbose(self, message):
        self.__log(self.VERBOSE, message)
