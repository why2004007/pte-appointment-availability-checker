# -*- coding: utf-8 -*-
# Created by Daniel Liu on 2017/7/25
import os
import smtplib


class MailAgent():
    def __init__(self, config, log):
        self.log = log
        self.config = config

    def sendEmail(self, message):
        try:
            self.log.debug('Send email: {0}'.format(message))

            # Gmail only supports ssl link.
            server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
            server_ssl.ehlo()
            username, password = self.config.getMailAuth()

            # Setup Mail Message Fields
            sentfrom = "{0} <{1}>".format(self.config.getSenderNickname(), username)
            sentTo = self.config.getReceivers()
            if self.config.isAdminReceiveNotification():
                sentTo.append(self.config.getAdminMail)
            if self.config.isDebug():
                sentTo = self.config.getAdminMail()
            subject = self.config.getMailSubject()
            body = message
            msg = """\
From: %s
To: %s
Subject: %s

%s
-----------------------------------------
Brought to you by Daniel Liu
Bug report: %s
                    """ % (sentfrom, sentTo, subject, body, self.config.getAdminMail())

            # Perform Send
            server_ssl.login(username, password)
            server_ssl.sendmail(sentfrom, sentTo, msg)
            server_ssl.close()
            self.log.debug('Email has been sent.')
        except Exception as e:
            self.log.error('Something went wrong...')
            self.log.error('{0}'.format(e))
            os._exit(1)
