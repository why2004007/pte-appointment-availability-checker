# PTE Appointement Avalability Check
PTE Appointement Avalability Check (PTE-AAC) is a project aims to help the test takers find their ideal timeslot for the examination. By subscribe to this service, the test takers will receive emails for the available timeslots.
	
# Prerequisite
* Linux Environment
* Python 3.6 and later
* Selenium and its python wrapper
* Chrome Developer Version
* Chrome Driver Developer Version

# Current Version
Version 1.1 Stable
	
# Current Feature
* Check Melbourne's test centers' availability.
* Send email notifications to subscribers.
* Config file support.
	
# Incoming Features
* Web front end for people to subscribe/unsubscribe or review test centers' state.
	
# License
Apache 2.0